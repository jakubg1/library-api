# Generated by Django 3.2.19 on 2023-06-06 09:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='borrowing',
            name='borrowing_date',
            field=models.DateField(auto_now_add=True),
        ),
    ]
