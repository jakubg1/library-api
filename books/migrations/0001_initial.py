# Generated by Django 3.2.19 on 2023-06-06 08:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Publishing',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=400)),
                ('country', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Publication',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=400)),
                ('description', models.CharField(blank=True, max_length=6000, null=True)),
                ('nb_of_pages', models.IntegerField(blank=True, null=True)),
                ('isBorrowed', models.BooleanField(blank=True, default=False)),
                ('authors', models.ManyToManyField(related_name='books', to='books.Author')),
                ('publishing', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='publication', to='books.publishing')),
            ],
        ),
        migrations.CreateModel(
            name='Borrowing',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('borrowing_date', models.DateField(auto_now=True)),
                ('return_date', models.DateField(blank=True, default=None, null=True)),
                ('payment', models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=6, null=True)),
                ('borrower', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='borrower', to=settings.AUTH_USER_MODEL)),
                ('librarian', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='librarian', to=settings.AUTH_USER_MODEL)),
                ('publication', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='books.publication')),
            ],
        ),
    ]
