from django.contrib import admin
from .models import Publication, Author, Borrowing, Publishing

admin.site.register(Author)
admin.site.register(Publishing)

@admin.register(Borrowing)
class BorrowingAdmin(admin.ModelAdmin):
    list_display = ('publication', 'librarian', 'borrower', 'return_date', 'borrowing_date', 'payment', 'pk')

@admin.register(Publication)
class PublicationAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'nb_of_pages', 'publishing', 'isBorrowed', 'pk')