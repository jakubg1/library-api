from rest_framework.permissions import BasePermission

class UserLibraryPermission(BasePermission):
    def has_permission(self, request, view):
        if view.action in ['list','retrieve']:
            return bool(request.user.is_authenticated and request.user.role >= 1)
        return bool(request.user.is_authenticated and (request.user.role >= 2 or request.user.is_superuser))


class BorrowingsPermission(BasePermission):
    def has_permission(self, request, view):
        if view.action in ['retrieve', 'list']:
            return bool(request.user.is_authenticated and request.user.role >= 1)
        return bool(request.user.is_authenticated and (request.user.role >= 3 or request.user.is_superuser))
    
    def has_object_permission(self, request, view, obj):
        if view.action in ['retrieve', 'list']:
            return bool(request.user.is_authenticated and (request.user.role >= 2 or request.user.is_superuser or obj.borrower == request.user))
        elif view.action in ['list']:
            return bool(request.user.is_authenticated and (request.user.role >= 2 or request.user.is_superuser))
        return bool(request.user.is_authenticated and (request.user.role  == 3 or request.user.is_superuser))