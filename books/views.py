from rest_framework.response import Response
from django.contrib.auth import get_user_model
from rest_framework.decorators import api_view, permission_classes
from authx.permissions import IsLibrarian
from .models import Publication, Borrowing, Author
from .serializers import BorrowingSerializer, BorrowingReturnSerializer, AuthorSerializer, UserBorrowingsSerializer, PublicationRepresentationSerializer
from django.http import HttpResponseNotFound
from datetime import datetime, date, timedelta
from math import floor
from authx.permissions import IsClient
from django.contrib.postgres.search import SearchVector, SearchRank, SearchQuery

User = get_user_model()

@api_view(['POST'])
@permission_classes([IsLibrarian])
def borrowBook(request):

    borrower = User.objects.filter(email=request.data.get('borrower')).first()
    librarian = request.user.id
    publication = Publication.objects.filter(id=request.data.get('publication'), isBorrowed=False).first()

    if not publication:
        return HttpResponseNotFound("Book not found or borrowed")

    if not borrower:
        return HttpResponseNotFound("Email not exists")

    serializer = BorrowingSerializer(data={"borrower":borrower.pk,"publication":publication.id,"librarian":librarian})

    if serializer.is_valid():
        publication.isBorrowed = True
        publication.save()
        instance = serializer.save()
        return Response(serializer.data)
    
    raise ValueError("Invalid data")


@api_view(['POST'])
@permission_classes([IsLibrarian])
def returnBook(request):

    
    publication = Publication.objects.filter(id=request.data.get('publication'), isBorrowed=True).first()

    if not publication:
        return HttpResponseNotFound("Book not found")
    
    borrowing = Borrowing.objects.filter(publication=publication.pk, return_date=None).order_by('-borrowing_date').first()

    if not borrowing:
        return HttpResponseNotFound("Trying to return not borrowed book")

    if borrowing.return_date:
        return HttpResponseNotFound("Book has been returned")
    
    publication.isBorrowed = False
    publication.save()
    borrowing.return_date = date.today()

    delta = (borrowing.return_date-borrowing.borrowing_date).days
    to_pay = (delta - 14) * 0.5 if delta > 14 else 0

    borrowing.payment = to_pay
    borrowing.save()

    return Response(BorrowingReturnSerializer(borrowing).data)


@api_view(['GET'])
@permission_classes([IsClient])
def getAuthorsByNameOrLastName(request):
    if request.query_params.get('searched_data'):
        authors = Author.objects.annotate(search=SearchVector('name', 'last_name')).filter(search=request.query_params.get('searched_data'))
        return Response(AuthorSerializer(authors, many=True).data)


@api_view(['GET'])
@permission_classes([IsClient])
def getMyBorrowings(request):
    user_id = request.user.id
    borrowings = Borrowing.objects.filter(borrower=user_id)
    return Response(UserBorrowingsSerializer(borrowings, many=True).data)

@api_view(['GET'])
@permission_classes([IsLibrarian])
def searchBorrowings(request):
    borrowings = Borrowing.objects.all()
    if request.query_params.get('user_id'):
        borrowings = borrowings.filter(borrower=request.query_params.get('user_id'))
    if request.query_params.get('publication_id'):
        borrowings = borrowings.filter(publication=request.query_params.get('publication_id'))
    if request.query_params.get('date_sorting') == 'asc':
        borrowings = borrowings.order_by('borrowing_date')
    elif request.query_params.get('date_sorting') == 'desc':
        borrowings = borrowings.order_by('-borrowing_date')

    return Response(UserBorrowingsSerializer(borrowings, many=True).data)

@api_view(['GET'])
@permission_classes([IsClient])
def searchPublications(request):
    if request.query_params.get('searched_data'):
        search_vector = SearchVector('title', weight='A') + SearchVector('description', weight='B')
        search_query = SearchQuery(request.query_params.get('searched_data'))
        results =  Publication.objects.all()
        if request.query_params.get('filter_borrowed'):
            results = results.filter(isBorrowed=False)
        results = results.annotate(rank=SearchRank(search_vector, search_query)).filter(rank__gte=0.1).order_by('-rank')
        serializer = PublicationRepresentationSerializer(results, many=True)
        return Response(serializer.data)    
    return Response({"status":"invalid searched data"})