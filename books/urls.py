from rest_framework.routers import DefaultRouter
from django.urls.conf import include
from django.urls import re_path, path
from .views import borrowBook, returnBook, getAuthorsByNameOrLastName, getMyBorrowings, searchBorrowings, searchPublications


from .viewsets import AuthorViewSet, PublishingViewSet, PublicationViewSet, BorrowingViewSet

router = DefaultRouter()

router.register(
    r'publishings',
    PublishingViewSet,
    basename='publishings',
)

router.register(
    r'authors',
    AuthorViewSet,
    basename='authors',
)

router.register(
    r'publications',
    PublicationViewSet,
    basename='publications',
)

router.register(
    r'borrowings',
    BorrowingViewSet,
    basename='borrowings',
)

urlpatterns = [
    path('searchAuthors/', getAuthorsByNameOrLastName, name='getAuthorsByNameOrLastName'),
    path('getMyBorrowings/', getMyBorrowings, name='getMyBorrowings'),
    path('searchBorrowings/', searchBorrowings, name='searchBorrowings'),
    path('searchPublications/', searchPublications, name='searchPublications'),
    path('borrowBook/', borrowBook, name="borrowBook"),
    path('returnBook/', returnBook, name="returnBook"),
    re_path(r'', include(router.urls)),
]