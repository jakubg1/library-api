from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Publication(models.Model):
    title = models.CharField(max_length=400, null=False, blank=False)
    description = models.CharField(max_length=6000, null=True, blank=True)
    nb_of_pages = models.IntegerField(null=True, blank=True)
    publishing = models.ForeignKey("Publishing", null=True, on_delete=models.SET_NULL, related_name="publication")
    authors = models.ManyToManyField("Author", related_name="books")
    isBorrowed = models.BooleanField(null=False, blank=True, default=False)

    def __str__(self):
        return self.title

class Publishing(models.Model):
    name = models.CharField(max_length=400, null=False, blank=False)
    country = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

class Author(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    last_name = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return self.name+" "+self.last_name
    
class Borrowing(models.Model):
    borrower = models.ForeignKey(User, blank=False, null=True, on_delete=models.SET_NULL, related_name="borrower")
    publication = models.ForeignKey("Publication", blank=False, null=True, on_delete=models.SET_NULL)
    librarian = models.ForeignKey(User, blank=False, null=True, on_delete=models.SET_NULL, related_name="librarian")
    borrowing_date = models.DateField(auto_now_add=True, null=False, blank=False)
    return_date = models.DateField(null=True, blank=True,default=None)
    payment = models.DecimalField(null=True, blank=True, default=0, decimal_places=2, max_digits=6)

    def __str__(self):
        return str(self.borrowing_date)+" - "+str(self.publication.title)+" - "+str(self.borrower.first_name+" "+self.borrower.last_name);
