from rest_framework import serializers
from .models import Publication, Publishing, Author, Borrowing
from authx.serializers import UserRepresentationSerializer

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('name', 'last_name', 'id')

class AuthorRepresentationSerializer(AuthorSerializer):
    class Meta:
        model = Author
        fields = ('name', 'last_name')





class PublishingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publishing
        fields = ('name', 'country', 'id')

class PublishingRepresentationSerializer(PublishingSerializer):
    class Meta:
        model = Publishing
        fields = ('name',)






class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ('title', 'description', 'nb_of_pages', 'publishing', 'authors','id')

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        rep['publishing'] = PublishingRepresentationSerializer(instance.publishing).data
        rep['authors'] = AuthorRepresentationSerializer(instance.authors, many=True).data
        return rep
    
class PublicationRepresentationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ('title', 'authors', 'id')

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        rep['authors'] = AuthorRepresentationSerializer(instance.authors, many=True).data
        return rep
    





class BorrowingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Borrowing
        fields = ('borrower', 'publication', 'librarian', 'id')

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        rep['publication'] = PublicationRepresentationSerializer(instance.publication).data
        rep['librarian'] = UserRepresentationSerializer(instance.librarian).data
        rep['borrower'] = UserRepresentationSerializer(instance.borrower).data
        return rep
    
class BorrowingReturnSerializer(BorrowingSerializer):
    class Meta:
        model = Borrowing
        fields = ('borrower', 'publication', 'librarian', 'borrowing_date', 'return_date', 'payment')

class UserBorrowingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Borrowing
        fields = ('publication', 'borrowing_date', 'return_date')

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        rep['publication'] = PublicationRepresentationSerializer(instance.publication).data
        return rep