from rest_framework.viewsets import ModelViewSet
from .serializers import AuthorSerializer, PublishingSerializer, PublicationSerializer, BorrowingSerializer
from .permissions import UserLibraryPermission, BorrowingsPermission
from .models import Author, Publishing, Publication, Borrowing

class AuthorViewSet(ModelViewSet):
    permission_classes = [UserLibraryPermission]
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()

class PublishingViewSet(ModelViewSet):
    permission_classes = [UserLibraryPermission]
    serializer_class = PublishingSerializer
    queryset = Publishing.objects.all()

class PublicationViewSet(ModelViewSet):
    permission_classes = [UserLibraryPermission] 
    serializer_class = PublicationSerializer
    queryset = Publication.objects.all()

class BorrowingViewSet(ModelViewSet):
    permission_classes = [BorrowingsPermission]
    serializer_class = BorrowingSerializer
    queryset = Borrowing.objects.all()

