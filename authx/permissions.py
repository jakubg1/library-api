from rest_framework.permissions import BasePermission

class IsClient(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.is_authenticated and request.user.role  >= 1)


class IsLibrarian(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.is_authenticated and request.user.role  >= 2)


class IsOwner(BasePermission):
    def has_permission(self, request, view):
        if view.action in ['retrieve', 'update', 'partial_update', 'destroy', 'perform_destroy']:
            return bool(request.user.is_authenticated and request.user.role >= 1)
        if view.action in ['create', 'perform_create']:
            return True
        return bool(request.user.is_authenticated and (request.user.role  == 3 or request.user.is_superuser))

    def has_object_permission(self, request, view, obj):
        if view.action in ['retrieve', 'update', 'partial_update', 'destroy', 'perform_destroy']:
            return bool(request.user.is_authenticated and (request.user.role >= 3 or request.user.is_superuser or obj == request.user))
        return bool(request.user.is_authenticated and (request.user.role  == 3 or request.user.is_superuser))