from django.utils.translation import ugettext_lazy as _

ROLES = (
    (1, _('Client')),
    (2, _('Librarian')),
    (3, _('Owner')),
)