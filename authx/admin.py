from django.contrib import admin
from .models import LibUser

@admin.register(LibUser)
class LibUserAdmin(admin.ModelAdmin):
    list_display = ['email', 'is_active', 'is_staff', 'is_superuser', 'role', 'pk']