from django.contrib.auth import get_user_model, logout
from rest_framework.viewsets import ModelViewSet
from django.core.mail import EmailMessage
from authx.serializers import UserSerializer
from django.utils.translation import ugettext_lazy as _
from authx.permissions import IsOwner
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_text
from .jwt import create_jwt
from datetime import datetime, timedelta

User = get_user_model()

class UserViewSet(ModelViewSet):
    queryset = User.objects.all() 
    serializer_class = UserSerializer
    permission_classes = [IsOwner]

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user.pk is instance.pk:
            logout(request)
        self.perform_destroy(instance)
        return Response(status=HTTP_204_NO_CONTENT)
    
    def send_auth_email(self, user):
        current_site = get_current_site(self.request)
        domain = current_site.domain,
        uid = urlsafe_base64_encode(force_bytes(user.pk)),
        token = default_token_generator.make_token(user)
        mail_subject = _('Activate your account - library')
        protocol = 'http'

        url = f"{protocol}://{domain[0]}/rest-api/v1/authx/activate/{uid[0]}/{token}"
        message = f'<p><a href="{url}">{url}</a></p>'
        email = EmailMessage(mail_subject,message, to=[user.email])

        email.send()

    def perform_create(self, serializer):
        user = serializer.save()
        user.is_active = False
        user.save()
        self.send_auth_email(user)


class LoginView(APIView):
    permission_classes = []
    authentication_classes = []

    def post(self, request):
        email = request.data['email']
        password = request.data['password']

        user = User.objects.filter(email=email).first()

        if user is None:
            raise AuthenticationFailed('User not found!')

        if not user.check_password(password):
            raise AuthenticationFailed('Incorrect password!')
        
        payload = {
            'id':user.id,
            'exp':datetime.now() + timedelta(minutes=60),
            'iat':datetime.now()
        }
        token = create_jwt(payload)
        return Response({'jwt':token})
    


class ActivationUserEmailView(APIView):
    permission_classes =  []
    authentication_classes = []

    def get(self, request, uidb64, token):
        uid = force_text(urlsafe_base64_decode(uidb64))
        print("DECODED UID: "+uid)
        try:
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and default_token_generator.check_token(user, token):

            user.is_active = True
            user.save()
            return Response(_('Thank you for your email confirmation. Now you can login your account.'), status=HTTP_204_NO_CONTENT)
        else:
            return Response(_('Activation link is invalid!'), status=HTTP_204_NO_CONTENT)

            
