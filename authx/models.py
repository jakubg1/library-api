from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser, BaseUserManager
from .common import ROLES

class LibUserManager(BaseUserManager):
    def create_user(self, email, password=None, role=1):
        if not email:
            raise ValueError('Please provide an email address')
        
        user = self.model(
            email=self.normalize_email(email), role=role
        )

        user.set_password(password)
        user.save()
        return user
    

    def create_superuser(self, email, password):
        user = self.create_user(email, password, 3)

        user.is_superuser = True
        user.is_admin = True
        user.is_staff = True
        user.save()

        return user
    
    def change_role(self, email, role=1):
        if not email:
            raise ValueError('Please provide an email address')

        user = super.get(email=email)

        if not user:
            raise ValueError('User not found')
        
        user.role = role
        user.save()
        

class LibUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=150, null=False, blank=True)
    last_name = models.CharField(max_length=150, null=False, blank=True)

    email = models.EmailField(max_length=255, unique=True, null=False, blank=False)
    phone = models.PositiveBigIntegerField(unique=True, null=True, blank=True)

    is_active = models.BooleanField(_('Is active user'), default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    role = models.PositiveIntegerField(_("User role"), choices=ROLES, default=1)

    join_date = models.DateTimeField(_("Join date"), auto_now_add=True)
    last_loggin = models.DateTimeField(_("Last loggin date"), auto_now_add=True)

    objects = LibUserManager()

    USERNAME_FIELD = 'email'

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __str__(self):
        return str(self.full_name)
    

